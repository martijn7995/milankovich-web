class OutputDisplay extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();

    // Create a shadow root
    var shadow = this.attachShadow({mode: 'open'});

    var style = document.createElement('link');
    style.setAttribute('rel','stylesheet');
    style.setAttribute('type','text/css');
    style.setAttribute('href','modules/output-display/output-display.css');
    
    var container = document.createElement('div');
    container.className = 'output-display-container';
    
    var loadSpinner = document.createElement('load-spinner');
    
    shadow.appendChild(style);
    shadow.appendChild(container);
    container.appendChild(loadSpinner);
  }
  initDom(){
    this.shadowRoot.querySelector('.output-display-container').replaceChild(inputValues.outputMethod.initDom(),this.shadowRoot.querySelector('load-spinner'));

  }
  draw(){
    inputValues.outputMethod.draw();
  }
  connectedCallback(){
    output.load(guiElements);
  }
}

// Define the new element
customElements.define('output-display', OutputDisplay);
