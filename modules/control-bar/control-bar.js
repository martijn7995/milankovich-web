class ControllBar extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();

    // Create a shadow root
    var shadow = this.attachShadow({mode: 'open'});

    //Create container
    var container = document.createElement('div');
    container.className = 'control-bar-container';
    
    //Create style sheet link
    var style = document.createElement('link');
    style.setAttribute('rel','stylesheet');
    style.setAttribute('type','text/css');
    style.setAttribute('href','modules/control-bar/control-bar.css');


    //Create a range element and set it's attributes.
    var rangeInput = document.createElement('input');
    rangeInput.setAttribute('type','range')
    rangeInput.setAttribute('min',0);
    rangeInput.setAttribute('max',100);
    rangeInput.setAttribute('value',0);
    rangeInput.setAttribute('time','0 kyr');
    rangeInput.setAttribute('step',1);
    rangeInput.className = 'control-bar-input custom-range';

    //Create playbutton
    var playButton = document.createElement('button');
    playButton.className = 'button icon-button play-button';
    
    //Create stepping buttons
    var playStepperButton = document.createElement('button');
    playStepperButton.className = 'button icon-button play-stepper-button';
    var forwards10Button = document.createElement('button');
    var forwards100Button = document.createElement('button');
    forwards10Button.className = 'button icon-button step-forward-button';
    forwards100Button.className = 'button icon-button step-fast-forward-button';
    var backwards10Button = document.createElement('button');
    var backwards100Button = document.createElement('button');
    backwards10Button.className = 'button icon-button step-backward-button';
    backwards100Button.className = 'button icon-button step-fast-backward-button';

    //Add all elements
    shadow.appendChild(style);
    shadow.appendChild(container);
    container.appendChild(rangeInput);
    container.appendChild(playButton);
    container.appendChild(backwards100Button);
    container.appendChild(backwards10Button);
    container.appendChild(playStepperButton);
    container.appendChild(forwards10Button);
    container.appendChild(forwards100Button);
    
    //bind elms
    inputValues.bind(this,'time','time');
    inputValues.bind(this,'min','minTime');
    inputValues.bind(this,'max','maxTime');

    //public variables
    this.rangeInput = rangeInput;
    this.playButton = playButton;
    this.stepperButton = playStepperButton;
    this.isPlaying = false;
    
    //event listeners
    rangeInput.addEventListener('input', function(e){
      var value = Number.parseFloat(this.value);
      guiElements.controlBar.setAttribute('time',value);
      guiElements.controlBar.rangeInput.setAttribute('time',util.getFromStep(value,Number.parseFloat(this.step))+' kyr vanaf J2000');
    });
    
    playButton.addEventListener('click', function(e){
      guiElements.controlBar.togglePlay();
    });

    backwards100Button.addEventListener('click', (e)=>{
      this.toggleStepper(-50)
    });
    backwards10Button.addEventListener('click', (e)=>{
      this.toggleStepper(-10)
    });
    playStepperButton.addEventListener('click', (e)=>{
      this.toggleStepper(1)
    });
    forwards10Button.addEventListener('click', (e)=>{
      this.toggleStepper(10)
    });
    forwards100Button.addEventListener('click', (e)=>{
      this.toggleStepper(50)
    });
  }
  
  //functions
  toggleStepper(step){
    this.stepperButton.classList.toggle('pause-stepper-button')

    if(guiElements.controlBar.isPlaying){
      guiElements.controlBar.stopStepper();
    }else{
      guiElements.controlBar.playStepper(step);
    }
  }
  playStepper(step){
    guiElements.controlBar.isPlaying=true;
    stepper.start(step);
  }
  stopStepper(){
    guiElements.controlBar.isPlaying=false;
    stepper.stop()
  }

  togglePlay(duration = 30){
    if(this.isPlaying){
      this.pause();
    }else{
      this.play(duration);
    }
  }
  play(duration = 50){
    if(!this.playButton.classList.contains('pause-button')){
      this.playButton.classList.add('pause-button');
    }
    if(this.playButton.classList.contains('play-button')){
      this.playButton.classList.remove('play-button');
    }
    this._startTime=null;
    this._startValue=this.time;
    this._timeDuration= (1-util.getProgress(this.time,this.min,this.max))*duration;
    this.isPlaying=true;
    
    if(this._timeDuration){
      window.requestAnimationFrame(this._playStep)
    }else{
      this.pause();
    }
  }
  pause(){
    if(!this.playButton.classList.contains('play-button')){
      this.playButton.classList.add('play-button');
    }
    if(this.playButton.classList.contains('pause-button')){
      this.playButton.classList.remove('pause-button');
    }
    this.isPlaying=false;
  }
  _playStep(timeStamp){
    var inst = guiElements.controlBar;
    if(!inst._startTime) inst._startTime = timeStamp;
    var progress = timeStamp - inst._startTime;
    progress*=0.001;
    inst.time= Math.min(inst._startValue + (inst.max - inst._startValue)*progress/inst._timeDuration,inst.max);
    if(inst.time < inst.max && inst.isPlaying){
      window.requestAnimationFrame(inst._playStep);
    }else{
      inst.pause();
    }
  }
  
  updateValues(){
    this.setAttribute('time',inputValues.time)
    this.setAttribute('min',inputValues.minTime)
    this.setAttribute('max',inputValues.maxTime)
  }
  
  //setters and getters
  set time(value){
    inputValues.time=value;
    this.setAttribute('time',value);
  }
  get time(){
    return Number.parseFloat(this.getAttribute('time'));
  }
  /*****/
  set min(value){
    inputValues.minTime=value;
    this.setAttribute('min',value)
  }
  get min(){
    return Number.parseFloat(this.getAttribute('min'));
  }
  /*****/
  set max(value){
    inputValues.maxTime=value;
    this.setAttribute('max',value)
  }
  get max(){
    return Number.parseFloat(this.getAttribute('max'));
  }
  /*****/
  set step(value){
    this.setAttribute('step',value);
  }
  get step(){
    return Number.parseFloat(this.getAttribute('step'));
  }
  
  //attribute watchers
  static get observedAttributes(){
      return ['time','step','min','max'];
  }

  attributeChangedCallback(attr, oldValue, newValue) {
    this.style.setProperty('--value', util.getProgress(this.time,this.min,this.max));
    if(util.isEqualToSome(attr,'min','max','step')){
      this.rangeInput.setAttribute(attr,newValue)
    }else if(attr == 'time'){
      var value = Number.parseFloat(newValue);
      this.rangeInput.value = value;
      if(guiElements.controlBar){
        guiElements.controlBar.setAttribute('time',value);      guiElements.controlBar.rangeInput.setAttribute('time',util.getFromStep(this.rangeInput.value,this.step)+' kyr vanaf J2000');
      }
    }
  }
  
  connectedCallback(){
      this.updateValues();
  }
}

// Define the new element
customElements.define('control-bar', ControllBar);
