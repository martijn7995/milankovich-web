class LoadSpinner extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();

    // Create a shadow root
    var shadow = this.attachShadow({mode: 'open'});

    var style = document.createElement('link');
    style.setAttribute('rel','stylesheet');
    style.setAttribute('type','text/css');
    style.setAttribute('href','modules/load-spinner/load-spinner.css');
    
    var div1 = document.createElement('div');
    var div2 = document.createElement('div');
    
    shadow.appendChild(style);
    shadow.appendChild(div1);
    shadow.appendChild(div2);
  }
}

// Define the new element
customElements.define('load-spinner', LoadSpinner);