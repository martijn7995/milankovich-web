class AbstractOutput{
  constructor(options){
    this.data=new Map();
    let defaultOptions = {
      name:  'Not named',
      requiredTables : [],
      updateEveryFrame : false,
    }
    options = Object.assign({},defaultOptions,options)
    this.name= options.name;
    this.requiredTables = options.requiredTables;
    this.updateEveryFrame = options.updateEveryFrame;

    this.isLoaded = false;


  }
  
  /*load in output display*/
  /*returns DOM for in output display*/
  load(){
    if(!this.data.size){
      this.loadTables();
    }
  }
  
  /*functions*/
  loadTables(){
    var loadedTables =[];
    console.log(this)
    this.requiredTables.forEach(table=>{
      if(!table.isLoaded){
        table.load().then(() =>{      
          loadedTables.push(table);
          if(loadedTables.length == this.requiredTables.length){
            //merge data
            this.initData();
            //trigger onload
            this.isLoaded = true;
            this.onLoadOutput();//for output load thingy
            this.onLoad();
            guiElements.outputDisplay.draw();
          }
        });
      }
    })
  }
  initTableProperties(){
    this.minTime = util.minKey(this.requiredTables);
    this.maxTime = util.maxKey(this.requiredTables)
    this.step = 0.1;
    guiElements.controlBar.setAttribute('min',this.minTime);
    guiElements.controlBar.setAttribute('max',this.maxTime);
    guiElements.controlBar.setAttribute('step',this.step);
    guiElements.controlBar.setAttribute('time',this.minTime);
  }
  
  initData(){
    this.requiredTables.forEach((table)=>{
      this.data = new Map([...this.data,...table.data]);
    });
    guiElements.controlBar.hidden=false;
    this.initTableProperties();
  }
  
  /*interface*/
  onLoad (){
    console.log('loaded interface');
    console.warn('Implement the onLoad on '+this.name);
  }
  draw(){
    var currentdate = new Date();
    var textTime = guiElements.outputDisplay.shadowRoot.querySelector('#time');
    textTime.textContent= new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
  }
  initDom(){
    var DOM = document.createElement('div');
    var textWarn = document.createElement('p');
    textWarn.className = 'warning';
    textWarn.innerHTML = 'please implement initDOM';

    var currentdate = new Date();
    var textTime = document.createElement('p');
    textTime.setAttribute('id','time')
    textTime.textContent= currentdate.getHours() + ":"
                          + currentdate.getMinutes() + ":"
                          + currentdate.getSeconds();
    DOM.appendChild(textWarn);
    DOM.appendChild(textTime);
    
    return DOM;
  }
  
}
