class CSVOutput extends AbstractOutput{
  constructor(){
    super({
      name: 'csv output',
      //requiredTables: [tables.la2004.past249MyrTo0]
      requiredTables: [tables.la2004.past51MyrTo0,tables.la2004.future0To21Myr],
      updateEveryFrame:false,
    });

    util.get('/data/Vostok/vostok.txt',(txt)=>{
      this.vostokData=txt;
      this.prepareVostok();

    })
    this.minTime=-422750;
    this.maxTime= 0;
    this.step = 10;
    this.vostokInitiated=false;
  }
  initTableProperties(){
  }
  onLoad (){
    console.log('hey!!!')
    this.prepareVostok()
  }
  prepareVostok(){
    if(!this.vostokInitiated){
      this.vostokInitiated=true
      return false;
    };
    this.vostokData=this.vostokData.split('\n');
    this.vostokData.forEach((row,i)=>{
      this.vostokData[i]=row.split('\t')
    })
    this.prepareTable();
  }
  prepareTable(){
    console.log('preparing')
    this.table=[];
    //time put vostok and milankovitch in this.table
    for(var time = this.minTime; time<=this.maxTime; time+=this.step){
      //loadVostok
      var vostok = (()=>{
        //find the rows above and under time given. if time exist in table floorIndex and ceilIndex are both equal to time else floorIndex<time<ceilIndex
        var floorIndex,ceilIndex;
        for(var index = 0; index<this.vostokData.length;index++){
          var curTime = -Number(this.vostokData[index][1]);
          var nextTime = -Number((this.vostokData[index+1] || this.vostokData[index])[1]);
          if(curTime>=time && nextTime<=time){
            if(curTime==time){
              floorIndex=ceilIndex=index;
              break;
            }
            if(nextTime==time){
              floorIndex=ceilIndex=index+1;
              break;
            }
            floorIndex = index<this.vostokData.length-1?index+1:index;
            ceilIndex = index
            break;
          }
        }
        var vostokFloor=this.vostokData[floorIndex].map(elm=>Number(elm)).filter((elm,i)=>i==1||i==3);
        var vostokCeil = this.vostokData[ceilIndex].map(elm=>Number(elm)).filter((elm,i)=>i==1||i==3);
        if(floorIndex==ceilIndex){
          return vostokCeil[1];
        }else{
          var vostokCeil = this.vostokData[ceilIndex].map(elm=>Number(elm)).filter((elm,i)=>i==1||i==3);
          return this.interpolateVostok(time, vostokCeil, vostokFloor);
        }
      })()
      var milankovitch = (()=>{
        var floorTime= Math.floor(time/1000);
        var ceilTime= Math.ceil(time/1000);
        if(floorTime==ceilTime){
          return this.data.get(floorTime);
        }else{
          return this.interpolateMilankovitch(time, this.data.get(floorTime), this.data.get(ceilTime));
        }
      })()
      var dataRow = (()=>{
        var temperature = vostok;
        var eccentricity = milankovitch['eccentricity'];
        var obliquity = milankovitch['obliquity'];
        var longitudePerihelion = milankovitch['longitudePerihelion'];
        return [time,eccentricity,obliquity,longitudePerihelion,temperature];
      })();
      this.table.push(dataRow);
    }
    //console.log(this.table)
  }
  //return value Temp
  interpolateVostok(time,floorArr,ceilArr){
    var value = util.interpolate(time, -floorArr[0], floorArr[1], -ceilArr[0], ceilArr[1]);
    return value;
  }
  //return Object
  interpolateMilankovitch(time,floorArr,ceilArr){
    var interpolatedArr= {};
    Object.keys(floorArr).forEach(key=>{
      interpolatedArr[key] = util.interpolate(time/1000, Math.floor(time/1000), floorArr[key], Math.ceil(time/1000), ceilArr[key]);
    })
    return interpolatedArr;
  }
  initDom(){
    var container = document.createElement('div');
    var button = document.createElement('button');
    button.textContent='generate download link';

    this.downloadLink= document.createElement('a');
    this.downloadLink.classList.add('downloadLink');
    this.downloadLink.setAttribute("download", 'milankovitchAndVostok.csv');

    button.addEventListener('click',e=>{
      var lineArray = [];
      //var header = 'data:text/csv;charset=utf-8,';
      var header = '"time","eccentricity","obliquity","longitudePerihelion","temperature"\n';
      this.table.forEach((infoArray, index)=>{
        var line = infoArray.join(",");
        lineArray.push(index == 0 ? header + line : line);
      });
      var csvContent = lineArray.join("\n");
      var csvFile = new Blob([csvContent],{type:'text/csv'})
      var encodedUri =  window.URL.createObjectURL(csvFile);
      this.downloadLink.setAttribute("href", encodedUri);
      this.downloadLink.textContent='download';
    })

    container.appendChild(button);
    container.appendChild(this.downloadLink);

    return container;
  }
  draw(){
  }
}
