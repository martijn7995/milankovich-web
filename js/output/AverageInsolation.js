class AverageInsolation extends AbstractOutput{
  constructor(){
    super({
      name: 'average insolation over year',
      //requiredTables: [tables.la2004.past249MyrTo0]
      requiredTables: [tables.la2004.past51MyrTo0,tables.la2004.future0To21Myr],
      updateEveryFrame:false,
    });
    this.minTime=-422750;
    this.maxTime= 422750;
    this.step = 10;
  }
  onLoad (){
    console.log('loaded average insolation');
    util.getInsolation()
  }
  //y = year in kyr ~~ φ = latitude
  getAverage(e, ε, 𝜛, φ){
    /*var milank = this.data.get(y)
    var e = milank['eccentricity'];
    var ε = milank['obliquity'];
    var 𝜛 = milank['longitudePerihelion'];*/

    var average = 0;
    var N = 500;
    for(var k=1;k<=N;k++){
      average+=util.getInsolation(e, ε, 𝜛, φ, this.getTrueAnomaly(365.25*k/N,e)+util.getInsolation(e, ε, 𝜛, φ, this.getTrueAnomaly(365.25*(k-1)/N,e)))
    }
    average = 1/(N)*average;
    //console.log(average)
    return average;
  }
  getTrueAnomaly(t,e){
    var M = 2*Math.PI/365.25*t;
    var E=M;
    for(var i=0; i<4; i++){
      E=M+e*Math.sin(E);
    }
    var θ = 2 * Math.atan(Math.sqrt((1+e)/(1-e)*Math.tan(E/2)**2));
    if(t > 182.625){
      θ = 2*Math.PI-θ;
    }
    return θ;
  }

  initDom(){
    var container = document.createElement('div');
    var container = document.createElement('div');
    var button = document.createElement('button');
    button.textContent='generate download link';

    //button.textContent = this.getAverage(0,0)
    this.downloadLink= document.createElement('a');
    this.downloadLink.classList.add('downloadLink');
    this.downloadLink.setAttribute("download", 'milankovitch-insolation.csv');

    button.addEventListener('click',e=>{
      var header = '"time","spring","summer","autumn","winter","average"';
      var lineArray = [header];
      this.data.forEach((row,time) => {
        var e = row['eccentricity'];
        var ε = row['obliquity'];
        var 𝜛 = row['longitudePerihelion'];

        var spring= util.getInsolation(e,ε,𝜛,util.toRadians(65),0)
        var summer= util.getInsolation(e,ε,𝜛,util.toRadians(65),Math.PI/2)
        var autumn= util.getInsolation(e,ε,𝜛,util.toRadians(65),Math.PI)
        var winter= util.getInsolation(e,ε,𝜛,util.toRadians(65),Math.PI/2*3)
        var average = this.getAverage(e, ε, 𝜛, util.toRadians(65))

        var line = [time*1000,spring,summer,autumn,winter,average].join(",");
        lineArray.push(line);
      });
      var csvContent = lineArray.join("\n");
      var csvFile = new Blob([csvContent],{type:'text/csv'})
      var encodedUri =  window.URL.createObjectURL(csvFile);
      this.downloadLink.setAttribute("href", encodedUri);
      this.downloadLink.textContent='download';
    })

    container.appendChild(button);
    container.appendChild(this.downloadLink);
    //container.innerHTML = this.getTrueAnomaly(100,0.0167)
    return container;
  }
  draw(){
  }
}
