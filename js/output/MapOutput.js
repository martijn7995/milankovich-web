class MapOutput extends AbstractOutput{
  constructor(){
    super({
      name: 'kaart kwartair',
      //requiredTables: [tables.la2004.past249MyrTo0]
      requiredTables: [tables.la2004.past51MyrTo0,tables.la2004.future0To21Myr],
    });
    this.onDrawFunctions=[];
  }
  onLoad (){
    console.info('loaded test output');
  }
  initTableProperties(){
    this.minTime = -422;
    this.maxTime =422;
    this.step = 0.001;
    guiElements.controlBar.setAttribute('min',this.minTime);
    guiElements.controlBar.setAttribute('max',this.maxTime);
    guiElements.controlBar.setAttribute('step',this.step);
    guiElements.controlBar.setAttribute('time',0);
  }
  initDom(){
    this.width = window.innerWidth;
    this.height = window.innerHeight-11*util.getRemValue();
    this.container = document.createElement('div');
    this.size = Math.min(this.width,this.height);
    this.svgMap = addD3Map(this.container,this.size,this.size)

    this.canvas = document.createElement('canvas');
    this.canvas.className = 'heatmap';
    this.canvas.width= this.size;
    this.canvas.height= this.size;
    this.canvas.style.left = `calc(50vw - ${this.size/2}px)`;

    var colorScale = document.createElement('div');
    colorScale.className = 'colorScale';
    var colorScaleText= document.createElement('ul');
    var numberOfTexts=6;
    for(var i = numberOfTexts; i>0; i--){
      var li = document.createElement('li');
      li.textContent= Math.round(475 / (numberOfTexts-1) * (i-1));
      console.log((i-1)/(numberOfTexts-1))
      li.style.setProperty('--list-progression',  (i-1)/(numberOfTexts-1));
      colorScaleText.appendChild(li);

    }
    colorScale.appendChild(colorScaleText);

    var colorScaleUnit = document.createElement('div');
    colorScaleUnit.innerHTML = 'W/m<sup>2</sup>';
    colorScaleUnit.classList.add('colorScaleUnit')
    colorScale.appendChild(colorScaleUnit);

    this.colorScaleIndicator = document.createElement('div');
    this.colorScaleIndicator.classList.add('colorScaleIndicator');
    this.colorScaleIndicator.setAttribute('data-value','190');
    this.colorScaleIndicator.style.setProperty('--value', 190)
    colorScale.appendChild(this.colorScaleIndicator);

    this.ctx = this.canvas.getContext('2d');
    this.container.appendChild(this.canvas);
    this.container.appendChild(colorScale);

    return this.container;
  }

  draw(){
    if(!this.ctx)
      return false;
    var imageData = this.ctx.createImageData(this.size,this.size);
    var data= imageData.data;
    var parameters = this.data.get(Math.round(inputValues.time));
    var max=-1000;
    var min=1000;
    //draw indicator
    (() => {
      let insolation = util.getInsolation(parameters['eccentricity'], parameters['obliquity'], parameters['longitudePerihelion'], util.toRadians(65),Math.PI/2*0);
      this.colorScaleIndicator.setAttribute('data-value',insolation.toFixed(0));
      this.colorScaleIndicator.style.setProperty('--value', insolation);
    })()
    //draw map
    for(var x = 0; x<this.size; x++){
      for(var y = 0; y<this.size; y++){
        var i = 4*(y*this.size +x);
        let coord = inputValues.projection.invert([x,y]); //[longitude, latitude]
        if(Number.isNaN(coord[0])||Number.isNaN(coord[1])){
          data[i+3]  = 0
        }else{
          let insolation = util.getInsolation(parameters['eccentricity'], parameters['obliquity'], parameters['longitudePerihelion'], util.toRadians(coord[1]),Math.PI/2*0);
          //max=Math.max(max,insolation);
          //min=Math.min(min,insolation);
          max=Math.max(max,util.toRadians(coord[1]),0);
          min=Math.min(min,util.toRadians(coord[1]),0);
          let color = util.heatMapColorforValue((insolation)/475);
          data[i]    = color[0];//TODO
          data[i+1]  = color[1];
          data[i+2]  = color[2];
          data[i+3]  = 255;
        }
      }
    }
    this.onDraw();
    console.log('max: '+max)
    console.log('min: '+min)
    this.ctx.putImageData(imageData,0,0);
  }

  addOnDraw(func){
    this.onDrawFunctions.push(func);
  }
  onDraw(){
    console.log('trigger')
    this.onDrawFunctions.forEach(func=>func());
  }
}
