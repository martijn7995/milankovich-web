function addD3Map(target,width,height){
  var rotation = [0,0]
  var scale = 0.95;
  var speed = 0.5;

  inputValues.projection = d3.geo.orthographic()
    .scale(Math.min(width,height)/2*scale)
    .translate([width / 2, height / 2])
    .clipAngle(90)
    .precision(.1);

  var path = d3.geo.path()
    .projection(inputValues.projection);

  var graticule = d3.geo.graticule();

  var svg = d3.select(target).append("svg")
    .attr("width", width)
    .attr("height", height);

  //load css file
  d3.text("/css/d3Map.css", function(error, text) {
    if (error) throw error;
    svg.append("style").text(text)
  });

  svg.append("defs").append("path")
    .datum({type: "Sphere"})
    .attr("id", "sphere")
    .attr("d", path);

  svg.append("use")
    .attr("class", "stroke")
    .attr("xlink:href", "#sphere");

  svg.append("use")
    .attr("class", "fill")
    .attr("xlink:href", "#sphere");

  svg.append("path")
    .datum(graticule)
    .attr("class", "graticule")
    .attr("d", path);

  //dragging effect
  svg.on("mousedown", function() {
    svg.classed("active", true);

    var w = d3.select(window)
        .on("mousemove", mousemove)
        .on("mouseup", mouseup);

    d3.event.preventDefault(); // disable text dragging
    function mousemove() {
      var p = [d3.event.movementX*speed,d3.event.movementY*speed]
      rotation = [rotation[0]+p[0],rotation[1]-p[1]]
      inputValues.projection.rotate(rotation);
      svg.selectAll("path").attr("d", path);
    }
    function mouseup() {
      svg.classed("active", false);
      w.on("mousemove", null).on("mouseup", null);
      window.requestAnimationFrame(guiElements.outputDisplay.draw);
    }
  });

  d3.json("/data/topojson/world-50m.json", function(error, world) {
    if (error) throw error;

    svg.insert("path", ".graticule")
      .datum(topojson.feature(world, world.objects.land))
      .attr("class", "land")
      .attr("d", path);

    svg.insert("path", ".graticule")
      .datum(topojson.mesh(world, world.objects.countries, function(a, b) { return a !== b; }))
      .attr("class", "boundary")
      .attr("d", path);
  });
  return svg.node();
}
