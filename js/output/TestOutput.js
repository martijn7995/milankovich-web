class TestOutput extends AbstractOutput{
  constructor(){
    super({
      name: 'test',
      //requiredTables: [tables.la2004.past249MyrTo0]
      requiredTables: [tables.la2004.past249MyrTo0,tables.la2004.past101MyrTo0,tables.la2004.future0To21Myr],
      updateEveryFrame:true,

    });

    this.columns = new Map(); //[name,keyName]
    this.columns.set('eccentriciteit (e)','eccentricity')
    this.columns.set('obliquiteit (ε, rad)','obliquity')
    this.columns.set('Lengtegraad van het perihelium (ϖ, rad)','longitudePerihelion')
  }
  onLoad (){
    console.info('loaded test output');
  }
  initTableProperties(){
    this.minTime = util.minKey(this.requiredTables);
    this.maxTime = util.maxKey(this.requiredTables)
    this.step = 0.1;
    guiElements.controlBar.setAttribute('min',this.minTime);
    guiElements.controlBar.setAttribute('max',this.maxTime);
    guiElements.controlBar.setAttribute('step',this.step);
    guiElements.controlBar.setAttribute('time',this.minTime);
  }
  initDom(){
    var time = inputValues.time;
    var dataNow = this.data.get(time);
    let keys = Array.from(this.columns.keys());
    let keyNames = Array.from(this.columns.values());

    this.table = document.createElement('table');
    this.table.innerHTML=`<thead><tr><th>${keys[0]}</th><th>${keys[1]}</th> <th>${keys[2]}</th></tr></thead>`;
    this.tableBody = document.createElement('tbody')
    this.tableBody.innerHTML=`<tr><td>${dataNow[keyNames[0]]}</td><td>${dataNow[keyNames[1]]}</td> <td>${dataNow[keyNames[2]]}</td></tr>`;
    this.table.appendChild(this.tableBody);
    return this.table;
    //this.data.forEach
  }
  draw(){
    if(this.table){
      var time = inputValues.time;
      var dataNow = this.data.get(time);
      let keyNames = Array.from(this.columns.values());
      console.log(time);
      var cells = this.tableBody.querySelectorAll('td');
      cells.forEach((cell,i)=>cell.textContent = dataNow[keyNames[i]])
    }
  }
}
