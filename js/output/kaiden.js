class KaidenOutput extends AbstractOutput{
  constructor(){
    super({
      name: 'min - max precession index',
      requiredTables: [tables.la2004.past101MyrTo0],
      updateEveryFrame: false,
    });
    this.timePeriods=[{name:'Elsterien',start:-465,end:-418},{name:'Saalien',start:-238,end:-126},{name:'Weichselien',start:-116,end:-11}]
  }

  initTableProperties(){
    guiElements.controlBar.hidden=true;
  }
  initDom(){
    this.table = document.createElement('table');
    this.table.innerHTML  = `<thead><tr><th>Precession index</th><th>minima</th><th>maxima</th></tr></thead>`;
    this.timePeriods.forEach(e=>{
      this.table.innerHTML += `<tr><th>${e.name}</th></tr>`;
    })
    return this.table;
  }
  draw(){
    this.table.innerHTML=`<thead><tr><th>Precession index</th><th>minima</th><th>maxima</th></tr></thead>`;
    this.timePeriods.forEach(timePeriod=>{
      let periodData = util.subMap(this.data, timePeriod.start, timePeriod.end)
      let precesionData = this.getPrecessionValues(periodData);
      let precesionDataVal = Array.from(this.getPrecessionValues(periodData).keys());
      let precesionMin= util.min(precesionDataVal);
      let precesionMax= util.max(precesionDataVal);

      let row = `<tr><th>${timePeriod.name}</th>`;
      row += `<td>${precesionMin} (${precesionData.get(precesionMin)} kyr)</td>`
      row += `<td>${precesionMax} (${precesionData.get(precesionMax)} kyr)</td></tr>`;
      this.table.innerHTML += row;
    })
  }

  getPrecessionValues(map){
    let prec = new Map();
    map.forEach((data,time)=>{
      prec.set(data.eccentricity * Math.sin(data.longitudePerihelion),time);
    })
    return prec;
  }

}
