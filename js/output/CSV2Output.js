class CSV2Output extends AbstractOutput{
  constructor(){
    super({
      name: 'csv 2 output',
      //requiredTables: [tables.la2004.past249MyrTo0]
      requiredTables: [tables.la2004.past51MyrTo0,tables.la2004.future0To21Myr],
      updateEveryFrame:false,
    });
    this.minTime=-422750;
    this.maxTime= 422750;
    this.step = 10;
  }
  initTableProperties(){
  }
  onLoad (){
    console.log('hey!!!')
    this.prepareTable();
  }
  prepareTable(){
    console.log('preparing')
    this.table=[];
    //time put milankovitch in this.table
    for(var time = this.minTime; time<=this.maxTime; time+=this.step){
      var milankovitch = (()=>{
        var floorTime= Math.floor(time/1000);
        var ceilTime= Math.ceil(time/1000);
        if(floorTime==ceilTime){
          return this.data.get(floorTime);
        }else{
          return this.interpolateMilankovitch(time, this.data.get(floorTime), this.data.get(ceilTime));
        }
      })()
      var dataRow = (()=>{
        var eccentricity = milankovitch['eccentricity'];
        var obliquity = milankovitch['obliquity'];
        var longitudePerihelion = milankovitch['longitudePerihelion'];
        return [time,eccentricity,obliquity,longitudePerihelion];
      })();
      this.table.push(dataRow);
    }
    //console.log(this.table)
  }
  //return Object
  interpolateMilankovitch(time,floorArr,ceilArr){
    var interpolatedArr= {};
    Object.keys(floorArr).forEach(key=>{
      interpolatedArr[key] = util.interpolate(time/1000, Math.floor(time/1000), floorArr[key], Math.ceil(time/1000), ceilArr[key]);
    })
    return interpolatedArr;
  }
  initDom(){
    var container = document.createElement('div');
    var button = document.createElement('button');
    button.textContent='generate download link';

    this.downloadLink= document.createElement('a');
    this.downloadLink.classList.add('downloadLink');
    this.downloadLink.setAttribute("download", 'milankovitch.csv');

    button.addEventListener('click',e=>{
      var lineArray = [];
      //var header = 'data:text/csv;charset=utf-8,';
      var header = '"time","eccentricity","obliquity","longitudePerihelion"\n';
      this.table.forEach((infoArray, index)=>{
        var line = infoArray.join(",");
        lineArray.push(index == 0 ? header + line : line);
      });
      var csvContent = lineArray.join("\n");
      var csvFile = new Blob([csvContent],{type:'text/csv'})
      var encodedUri =  window.URL.createObjectURL(csvFile);
      this.downloadLink.setAttribute("href", encodedUri);
      this.downloadLink.textContent='download';
    })

    container.appendChild(button);
    container.appendChild(this.downloadLink);

    return container;
  }
  draw(){
  }
}
