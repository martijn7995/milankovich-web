class TableValue{
  constructor(startByte,stopByte,format,unit,label,description){
    this.startByte = startByte;
    this.stopByte = stopByte;
    this.format = format;
    this.unit = unit;
    this.label = label;
    this.description = description;
  }
}
class Table{
  //string, TableValue[]
  constructor(url, tableValueList){
    this.url = url;
    this.tableValueList = tableValueList;
    this.isLoaded = false;
    this.minTime;
    this.maxTime;
  }

  get data(){
    if(this._data){
      return this._data
    }else{
      throw new Error("Table hasn't loaded")
    }
  }
  set data(val){
    throw new Error('Data is read-only');
  }

  load(){
    return new Promise(
      (resolve, reject) => { 
        var worker = new Worker('js/worker.js');
        worker.addEventListener('message', e => {
          this._data = e.data; this.isLoaded = true ; resolve(this)
          this._data = e.data;
          this.isLoaded = true;
          this.initProperties();
          resolve(this);
        }, false);
        worker.postMessage([this]);
      }
    )
  }

  initProperties(){
    var dataKeys =  Array.from(this.data.keys());
    this.minTime = util.min(dataKeys);
    this.maxTime = util.max(dataKeys);
  }
}
