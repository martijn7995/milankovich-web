var stepper = {
  stopped : true,
  //frames: [],
  start(step=1){
    this.stopped = false;
    inputValues.outputMethod.addOnDraw(function(){
      if((Math.sign(step)>0 && inputValues.timeProgress <= 1)||(Math.sign(step)<0 && inputValues.timeProgress>= 0)){
        if(!this.stopped)
          setTimeout(function(){window.requestAnimationFrame(function(){
            inputValues.time+=step
          })},500);
      }else{
        stepper.stop()
      }
    });

    inputValues.time=inputValues.time;
  },
  stop(){
    this.stopped = true;
    inputValues.outputMethod.onDrawFunctions=[];
  }
}
