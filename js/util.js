class util {
  static ajax(url, method, params, callback){
    if (typeof callback == 'undefined') {
      callback = arguments[2];
      params = null;
    }
    var ajax = new XMLHttpRequest();

    ajax.onreadystatechange = function() {
      if (ajax.readyState == 4){
        if (ajax.status == 200){
          if (typeof callback == 'function') {
            callback(ajax.responseText);
          }
        }
      }
    }

    ajax.open(method, url, true);
    if (params) {
      ajax.send(params);
    } else {
      ajax.send();
    }        
  }

  static get(url, params, callback) {
   this.ajax(url, 'GET', params, callback);
  }

  static post(url, params, callback) {
   this.ajax(url, 'POST', params, callback);
  }
  // return the constrained number between min and max
  static constrain(value, min,max){
   return Math.min(Math.max(value,min),max)
  }
  // return number between 0 and 1
  static getProgress(value, min,max){
   return (value - min)/(max-min);
  }
  // return number between 0 and 1
  static getFromProgress(progress, min,max){
   return min + progress * (max - min);
  }
  //returns linear interpolated number
  static interpolate(x,ax,ay,bx,by){
    var val = ay+(by-ay)*((x-ax)/(bx-ax));
    if(Number.isNaN(val))
      debugger;
    return ay+(by-ay)*((x-ax)/(bx-ax));
  }
  // return a number step*n closest to value
  // if step is 0 return value direct
  static getFromStep(value,step){
    var ans = Math.round(value/step)*step;
    ans = ans.toFixed(util.getNumPrecision(step));
    return Number.isNaN(ans)? value : ans;
  }
  // return amount of decimals
  static getNumPrecision(aNum) {
    if (!Number.isFinite(aNum)) return 0;
    var e = 1;
    while (Math.round(aNum * e) / e !== aNum) e *= 10;
    return Math.log10(e);
  }
  
  //helper equal functions
  static isEqualToAll(...args){
    return args.every(value=>value===args[0]);
  }
  static isEqualIgnoreCaseToAll(...args){
    args = args.map(value =>  typeof value == 'string' ? value.toUpperCase() : value);
    return util.isEqualtoAll.apply(this,args);
  }
  static isEqualToSome(...args){
    return (args.slice(1)).some(value=>value===args[0]);
  }
  static isEqualIgnoreCaseToSome(...args){
    args = args.map(value =>  typeof value == 'string' ? value.toUpperCase() : value);
    return util.isEqualToSome.apply(this,args);
  }
  
  //return the smallest value in an array (for large arrays)
  static min(arr){
    var smallest = arr[0];

    for(var i=1; i<arr.length; i++){
        if(arr[i] < smallest){
            smallest = arr[i];
        }
    }
    return smallest;
  }
  //return the biggest value in an array (for large arrays)
  static max(arr){
    var largest = arr[0];

    for(var i=1; i<arr.length; i++){
        if(arr[i] > largest){
            largest = arr[i];
        }
    }
    return largest;
  }

  //returns smallest key for a map array
  static minKey(arrMap){
    return arrMap.reduce((acc,cur)=>Math.min(acc,cur.minTime),Infinity);
  }
  //returns smallest key for a map array
  static maxKey(arrMap){
    return arrMap.reduce((acc,cur)=>Math.max(acc,cur.maxTime),-Infinity);
  }
  //
  static subMap(map, min, max){
    var temp = new Map();
    Array.from(map.keys()).filter(key=>key>=min&&key<=max).forEach(key=>{
      temp.set(key,map.get(key));
    })
    return temp;
  }

  //returns rem value in px
  static getRemValue(){
    if(document.body){
      return Number(getComputedStyle(document.body,null).fontSize.replace(/[^\d]/g, ''));
    }else{
      return 16;
    }
  }

  //calculate insolation
  static getInsolation(e,ε,𝜛,φ,θ){//e:eccentricity,ε:obliquity,𝜛:longitude of perihelion,φ:latitude on earth,θ: polar angle
    let δ = ε * Math.sin(θ);
    //δ = Math.asin(Math.sin(ε)*Math.sin(θ-𝜛));
    let temp = Math.tan(φ)*Math.tan(δ);
    let h0 = 0;
    if(temp>1){
      h0 = Math.PI//h0
    }else if(temp<-1){
      return 0;
    }else{
      h0 = Math.acos(-temp);
    }
    let R=1+e*Math.cos(θ-𝜛); //(R0/RE)
    let S=1367 / Math.PI //S0 / pi
    let ans = S  * R**2 * (h0*Math.sin(φ) * Math.sin(δ) + Math.cos(φ) * Math.cos(δ) * Math.sin(h0))
    if(ans<0){
      debugger;
    }
    return ans;
  }

  static toRadians(degrees){
    return degrees * (Math.PI/180);
  }

  static heatMapColorforValue(value){
    value = util.constrain(value,0,1);
    var h = (1.0 - value) * .6667;
    return util.hslToRgb(h,1.0,0.5);
  }

  static hslToRgb(h, s, l){
    var r, g, b;

    if(s == 0){
        r = g = b = l; // achromatic
    }else{
        var hue2rgb = function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}

  static ajaxTable(table){
    return new Promise(function(resolve, reject){
      var req = new XMLHttpRequest();
      req.open('GET', table.url);
      
      req.onload = function(){
        if(req.status == 200){
          var response = req.responseText;
          var lineLength = table.tableValueList[table.tableValueList.length-1].stopByte+1;
          var linesAmount = response.length/lineLength;
          var data=[];
          for(var i = 0; i<linesAmount;i++){
            data[i]={};
            table.tableValueList.forEach(function(element){
              var value=response.substring(element.startByte+i*lineLength,element.stopByte+i*lineLength); //get raw values (string)
              if(value.includes('D')){
                value=value.split('D').map(function(num){
                  return Number.parseFloat(num)
                });
                value=value[0]*10**value[1]                
              }else{
                value = Number.parseFloat(value);
              }
              data[i][element.description]=value;
            })
          }
          table.tableValueList.startByte;
          resolve(data)
        }else{
          reject(Error(req.statusText));
        } 
      }
      
      req.onerror = function(){
        reject(Error("Network Error"));
      }
      
      req.send();
    })
  } 
}
