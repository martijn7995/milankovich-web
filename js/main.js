//init
const tables= {
  la2010d:{
    alkhqp3: '/data/La2010/La2010d_alkhqp3.dat',
    alkhqp3L:'/data/La2010/La2010d_alkhqp3L.dat',
    ecc3: '/data/La2010/La2010d_ecc3.dat',
    ecc3L:'/data/La2010/La2010d_ecc3L.dat',
  },
  la2004:{
    future0To21Myr: new Table('/data/La2004/INSOLP.LA2004.BTL.ASC', 
                              [new TableValue(1,14,'F13.3','kyr','t','time'),
                               new TableValue(18,39,'D25.16','','e','eccentricity'),
                               new TableValue(43,64,'D25.16','rad','ε','obliquity'),
                               new TableValue(68,89,'D25.16','rad','ϖ','longitudePerihelion')]),
    past51MyrTo0:  new Table('/data/La2004/INSOLN.LA2004.BTL.ASC', 
                              [new TableValue(1,14,'F13.3','kyr','t','time'),
                               new TableValue(18,39,'D25.16','','e','eccentricity'),
                               new TableValue(43,64,'D25.16','rad','ε','obliquity'),
                               new TableValue(68,89,'D25.16','rad','ϖ','longitudePerihelion')]),
    past101MyrTo0: new Table('/data/La2004/INSOLN.LA2004.BTL.100.ASC', 
                              [new TableValue(1,9,'F8.0','kyr','t','time'),
                               new TableValue(10,18,'D9.6','','e','eccentricity'),
                               new TableValue(19,27,'D9.6','rad','ε','obliquity'),
                               new TableValue(28,37,'D10.6','rad','ϖ','longitudePerihelion')]),
    past249MyrTo0: new Table('/data/La2004/INSOLN.LA2004.BTL.250.ASC', 
                              [new TableValue(1,9,'F8.0','kyr','t','time'),
                               new TableValue(10,18,'D9.6','','e','eccentricity'),
                               new TableValue(19,27,'D9.6','rad','ε','obliquity'),
                               new TableValue(28,37,'D10.6','rad','ϖ','longitudePerihelion')]),
  }
}

const guiElements =  {
  outputDisplay:null,
  controlBar:null,
  
  init:function(){
    this.outputDisplay = document.querySelector('output-display');
    this.controlBar = document.querySelector('control-bar');
  }
}
//domReadyi
document.addEventListener('DOMContentLoaded',function(){
  guiElements.init();
  /*
inputValues.bind(document.getElementById('input'),'value','time');
inputValues.bind(document.getElementById('input'),'min','minTime');
inputValues.bind(document.getElementById('input'),'max','maxTime');*/
})

/*register outputs*/
output.registerOutput(new MapOutput());
output.registerOutput(new AverageInsolation());
output.registerOutput(new CSV2Output());
output.registerOutput(new CSVOutput());
output.registerOutput(new KaidenOutput());
output.registerOutput(new TestOutput());
