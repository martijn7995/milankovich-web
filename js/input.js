var _inputHandler= {
  _outputUpdateValues : ['time','minTime','maxTime'],
  time : 0, //2-way || constrained
  minTime : -51, //2 way (input._minTime <= input._maxTime)
  maxTime : 0, //2 way (input._maxTime >= input._minTime)
  outputMethodIndex : 0, //normal variable
  updateOutput: function(){
    if(!inputValues.outputMethod.updateEveryFrame&&inputValues.outputMethod.isLoaded){
      output.draw();
    }
  },
}
var _inputBinding = {
  //2-way binding for input
  bind: function(elm,attr,keyName){
    if(!(keyName in this)){
      return false;
    }

    if(attr === 'value'){
      elm.addEventListener('input',e=>{ //elm.value -> input.value
        console.log('update elm value: elm.value->input.value');
        this[keyName] = e.target.value;
      })
    }else{
      this._observer.observe(elm,this._observerConfig)
    }

    //put in Map
    if(this._boundKeys.has(keyName)){
    	this._boundKeys.get(keyName).push({elm:elm,attr:attr});

    }else{
    	this._boundKeys.set(keyName,[{elm:elm,attr:attr}]);
    }
  },
  _boundKeys : new Map(), //keyName:{elm,attr}[]
  _observer: new MutationObserver(function(mutations){
    //foreach binding with binding.attr==attribute
    mutations.forEach(mutation =>{
      var attribute = mutation.attributeName;
      Array.from(inputValues._boundKeys.entries()).forEach(entry=>{
        entry[1].forEach(binding =>{
          if(binding.attr==attribute){
            var value = binding.elm.getAttribute(attribute);
            var keyName = entry[0];
            if(typeof inputValues[keyName]==='number'){
              value = Number.parseFloat(value);
            }
            if(inputValues[keyName] != value){
              inputValues[keyName] = value;
              console.log('update elm value: elm.attr -> input.elm')
            }
          }
        })
      })
    })
  }),
  _observerConfig: {attributes: true},
  _checkBinding : function (key,value){
    if(!this._boundKeys.has(key)){
      return false;
    }
    var bindings = this._boundKeys.get(key); //{elm,attr}[]
    if(bindings.some(e=>e.attr === 'value')){
      bindings.filter(e=>e.attr === 'value').forEach(binding=>{
        if(binding.elm.value!=value){
          binding.elm.value=value;
          console.log('update elm value: input.value -> elm.value')
        }
      });
    }else{
      bindings.forEach(binding=>{
        if(binding.elm.getAttribute(binding.attr)!=value){
          binding.elm.setAttribute(binding.attr,value)
          console.log(`update elm value: input.${key} -> elm.${binding.attr}`)
        }
      });
    }
  },
}

//new test  inputvalue but then proxy
var inputValues = new Proxy(Object.assign(_inputHandler,_inputBinding),

{//handler
  set(target, key, value){
    this.invariant(key,'set');

    switch(key){
      case 'time': //constrain
        value = util.constrain(value,target.minTime,target.maxTime);
        break;
      case 'timeProgress'://put time value in input.time
        value = util.constrain(value,0,1);
        value = util.getFromProgress(value,target.minTime,target.maxTime);
        inputValues.time = value;
        return true;
        break;
      case 'minTime': //input._minTime <= input._maxTime
        if(value>target.maxTime){
          target[key] = target.maxTime;
          target.maxTime=value;
        }
        break;
      case 'maxTime':
        if(value<target.minTime){
          target[key] = target.minTime;
          target.minTime=value;
        }
        break;
      case 'outputMethodIndex':
        target[key] = value;
        output.load(guiElements.outputDisplay);
        break;
      case 'outputMethod':
        if(output.registeredOutputs.indexOf(value)==-1){
          console.warn('Not found output method: '+value)
        }
        inputValues.outputMethodIndex =Math.max(0,output.registeredOutputs.indexOf(value));
        break;
    }
    target[key] = value;

    target._checkBinding(key, value);
    if(target._outputUpdateValues.includes(key)){
      target.updateOutput();
    }
    return true;
  },
  get (target, key){
    this.invariant(key,'get');
    switch(key){
      case 'timeProgress':
        return util.getProgress(target.time,target.minTime,target.maxTime);
        break;
      case 'outputMethod':
        return output.registeredOutputs[target.outputMethodIndex];
        break;
    }

    return target[key];
  },



  invariant: function(key,action){
    if(key[0]==='_'){
      //console.warn(`Attempt to ${action} private "${key}" property`)
      //throw new Error(`Invalid attempt to ${action} private "${key}" property`);
    }
  }

});
