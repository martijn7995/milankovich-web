self.importScripts('util.js');
self.addEventListener('message', function(e) {
  var table = e.data[0];
  util.get(table.url,function(response){
    var lineLength = table.tableValueList[table.tableValueList.length-1].stopByte+1;
    var linesAmount = response.length/lineLength;
    var data = new Map();
    for(var i = 0; i<linesAmount;i++){
      var dataLine = {};
      table.tableValueList.forEach(function(element){        
        var value=response.substring(element.startByte+i*lineLength,element.stopByte+i*lineLength); //get raw values (string)
        //parse string to float
        if(value.includes('D')){
          value=value.split('D').map(function(num){
            return Number.parseFloat(num)
          });
          value=value[0]*10**value[1];
        }else{
          value = Number.parseFloat(value);
        }
        //add value to array
        dataLine[element.description] = value;
      })
      
      var time = dataLine['time'];
      delete dataLine['time']
      data.set(time,dataLine);
    }
    table.tableValueList.startByte;
    self.postMessage(data);
    
  })
}, false);