var output = {
  load: function(outputDisplay){
    inputValues.outputMethod.load();
    inputValues.outputMethod.onLoadOutput=function(){
      //inputValues.outputMethod.initInput();
      guiElements.outputDisplay.initDom();
      window.requestAnimationFrame(output.draw);

    }
  },
  draw: function(){
    guiElements.outputDisplay.draw();
    if(inputValues.outputMethod.updateEveryFrame){
      window.requestAnimationFrame(output.draw);
    }
  },
  registerOutput : function (method){
    this.registeredOutputs.push(method);
  },
  registeredOutputs: [],
}
